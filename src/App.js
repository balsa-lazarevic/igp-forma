import React from 'react';
import Form from './components/Form.js'

import './App.css';

function App() {
  return (
    <div className="App">
      <div className="wrapper">
        <Form />
      </div>
    </div>
  );
}

export default App;
