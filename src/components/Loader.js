import React from 'react'
import ReactDOM from 'react-dom'

class Loader extends React.Component {
  render () {
    return (
        <div className="loader">
            <div className="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
        </div>
    )
  }
}

export default Loader;

