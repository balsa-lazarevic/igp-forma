import React from 'react'
import Loader from './Loader'

class Form extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            step: 1,
            fields: {
                fname: {
                    code: 'fname',
                    name: 'First name',
                    dataType: 'string',
                    value: '',
                    validators: {
                        min: '2',
                        max: '25'
                    },
                    validated: false,
                    highlight: false
                },
                lname: {
                    code: 'lname',
                    name: 'Last name',
                    dataType: 'string',
                    value: '',
                    validators: {
                        min: '2',
                        max: '25'
                    },
                    validated: false,
                    highlight: false
                },
                email: {
                    code: 'email',
                    name: '',
                    dataType: 'string',
                    value: '',
                    validators: {
                        re: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                    },
                    validated: false,
                    highlight: false
                },
                terms: {
                    value: false,
                    validators: {
                        required: true
                    },
                    validated: false,
                    highlight: false
                },
                username: {
                    code: 'username',
                    name: 'Username',
                    dataType: 'string',
                    value: '',
                    validators: {
                        min: '4',
                        max: '20'
                    },
                    validated: false,
                    highlight: false
                },
                password: {
                    code: 'password',
                    name: 'Password',
                    dataType: 'string',
                    value: '',
                    validators: {
                        re: /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,16})/
                    },
                    validated: false,
                    highlight: false
                },
                passwordConfirm: {
                    code: 'passwordConfirm',
                    name: 'Password Confirmation',
                    dataType: 'string',
                    value: '',
                    validators: {
                        re: /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,16})/
                    },
                    validated: false,
                    highlight: false
                }
            },
            translations: {
                lang: 'en',
                en: {
                    title: 'Monkey Business Casino',
                    create: 'Create Account',
                    created: 'Account Created!',
                    next: 'Next',
                    back: 'Back',
                    accept: 'I accept ',
                    terms: 'Terms and Conditions',
                    fname: 'First name',
                    lname: 'Last name',
                    email: 'Email',
                    username: 'Username',
                    password: 'Password',
                    passwordConfirm: 'Password Confirm'
                },
                hr: {
                    title: 'Monkey Business Kazino',
                    create: 'Kreirajte Nalog',
                    created: 'Nalog Kreiran!',
                    next: 'Dalje',
                    back: 'Nazad',
                    accept: 'Prihvatam ',
                    terms: 'Uslove Korištenja',
                    fname: 'Vaše ime',
                    lname: 'Vaše prezime',
                    email: 'Vaš Email',
                    username: 'Korisničko ime',
                    password: 'Lozinka',
                    passwordConfirm: 'Potvrdite lozinku'
                }
            }
        }

        this.nextStep = this.nextStep.bind(this);
        this.stepBack = this.stepBack.bind(this);
        this.validate = this.validate.bind(this);
        this.validated = this.validated.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.validateField = this.validateField.bind(this);
        this.prepareSubmit = this.prepareSubmit.bind(this);
        this.changeLanguage = this.changeLanguage.bind(this);
        this.submitRegistration = this.submitRegistration.bind(this);
    }

    handleChange(e, fieldName) {
        var updateValue = e.target.value;

        //If checkbox
        if (fieldName == 'terms') { updateValue = !this.state.fields.terms.value }

        this.setState(prevState => ({
            fields: {
                ...prevState.fields,
                [fieldName]: {
                    ...prevState.fields[fieldName],
                    value: updateValue
                }
            }
        }), function () {
            //Validate field after update
            this.validate(fieldName);
        })
    }

    validateField(fieldName, validation) {
        if (validation) { var toHighlight = false; var toValidate = true; }
        else { var toHighlight = true; var toValidate = false; }

        this.setState(prevState => ({
            fields: {
                ...prevState.fields,
                [fieldName]: {
                    ...prevState.fields[fieldName],
                    highlight: toHighlight,
                    validated: toValidate
                }
            }
        }))
    }

    //Validate single field
    validate(fieldName) {
        var field = this.state.fields[fieldName];
        var validators = field.validators;

        var valid = true;

        if (validators.min) { if (field.value.length < validators.min) { valid = false; } }
        if (validators.max) { if (field.value.length > validators.max) { valid = false; } }
        if (validators.re) { if (!validators.re.test(field.value)) { valid = false; } }
        if (validators.required) { if (!field.value) { valid = false; } }

        //Match passwords
        if (fieldName == 'passwordConfirm') { if (field.value !== this.state.fields.password.value) { valid = false; } }

        if (valid) { this.validateField(fieldName, true) } else { this.validateField(fieldName, false) }
    }

    //Return validation for multiple fields - for continuing step
    validated(fieldNames) {
        var returnValidation = true;

        //To reference properly in next scope
        var that = this;
        fieldNames.forEach(function (fieldName) {
            if (!that.state.fields[fieldName].validated) {
                returnValidation = false;
            }
        })

        return returnValidation;
    }

    //Return validation for multiple fields - for continuing step
    submitRegistration(fields) {
        console.log(fields)
        return new Promise((resolve, reject) => {
            //success
            setTimeout(function () {
                resolve({ success: true });
            }, 1000);
        });
    }

    prepareSubmit() {
        var returnFields = { fields: [] };

        var allFields = this.state.fields;

        for(var fieldName in allFields) {
            var field = (allFields[fieldName]);
            
            var object = {
                code: field.name,
                valueStr: field.value,
                dataType: field.dataType
            };

            returnFields.fields.push(object);
        }

        return returnFields;
    }

    async nextStep(e, step) {
        e.preventDefault();

        this.loader();
        var nextStep = step;

        //Fields to check
        if (step == 2) {
            var fields = ['fname', 'lname', 'email', 'terms'];
        }
        else if (step == 3) {
            var fields = ['username', 'password', 'passwordConfirm'];
        }

        //Validate
        if (!this.validated(fields)) { nextStep -= 1; }

        //Simulate waiting
        if (step == 2) {
            setTimeout(() => {
                this.setState({ step: nextStep })
            }, 2000);
        }
        else if (step == 3) {
            this.submitRegistration(this.prepareSubmit()).then( result => {
                if(result.success){
                    this.setState({ step: nextStep })
                }
                else {
                    nextStep -= 1;
                    this.setState({ step: nextStep })
                }
            });
        }
    }

    stepBack(e, step) {
        e.preventDefault();
        this.setState({ step })
    }

    loader() {
        this.setState({ step: 0 })
    }

    translation(term) {
        var lang = this.state.translations.lang;
        var translations = this.state.translations[lang];

        return translations[term];
    }

    changeLanguage(lang) {
        this.setState(prevState => ({
            translations: {
                ...prevState.translations,
                lang
            }
        }))
    }

    render() {

        return (
            <div className="register-card">
                <div className="translations">
                    <img src="/assets/translations/en.png" className="translation" onClick={() => this.changeLanguage('en')} />
                    <img src="/assets/translations/hr.png" className="translation" onClick={() => this.changeLanguage('hr')} />
                </div>
                <div className="logo-wrapper">
                    <img src="/assets/logo/banana-logo.png" className="logo" />
                    <p className="logo-text">{ this.translation('title') }</p>
                </div>
                <div className="heading">
                    <button className={this.state.step !== 2 ? 'hide' : 'btn btn-back'} onClick={e => this.stepBack(e, 1)}>{ this.translation('back') }</button>
                    <p className="main-heading">
                        {this.state.step !== 3 ? this.translation('create') : this.translation('created') }
                    </p>
                </div>
                <div className="form-wrapper">
                    <form>
                        <div className={this.state.step !== 1 ? 'hide' : ''}>
                            <div className="row">
                                <input type="text" name="fname" placeholder={ this.translation('fname') } className={`text-input half-input ${this.state.fields.fname.highlight ? 'highlight' : ''}`} onChange={e => this.handleChange(e, 'fname')}></input>
                                <input type="text" name="lname" placeholder={ this.translation('lname') } className={`text-input half-input ${this.state.fields.lname.highlight ? 'highlight' : ''}`} onChange={e => this.handleChange(e, 'lname')}></input>
                            </div>
                            <div className="row">
                                <input type="email" name="email" placeholder="Email" className={`text-input ${this.state.fields.email.highlight ? 'highlight' : ''}`} onChange={e => this.handleChange(e, 'email')}></input>
                            </div>
                            <div className="row">
                                <input type="checkbox" name="terms" className={`checkbox-input ${this.state.fields.terms.highlight ? 'highlight' : ''}`} onChange={e => this.handleChange(e, 'terms')} /><p className="terms-text">{ this.translation('accept') }<a href="##" className="terms-link">{ this.translation('terms') }</a></p>
                            </div>
                            <button className="btn btn-next" onClick={e => this.nextStep(e, 2)}>{ this.translation('next') }</button>
                        </div>
                        <div className={this.state.step !== 0 ? 'hide' : ''}>
                            <Loader />
                        </div>
                        <div className={this.state.step !== 2 ? 'hide' : ''}>
                            <div className="row">
                                <input type="text" name="username" placeholder={ this.translation('username') } className={`text-input ${this.state.fields.username.highlight ? 'highlight' : ''}`} onChange={e => this.handleChange(e, 'username')}></input>
                                <input type="password" name="password" placeholder={ this.translation('password') } className={`text-input half-input ${this.state.fields.password.highlight ? 'highlight' : ''}`} onChange={e => this.handleChange(e, 'password')}></input>
                                <input type="password" name="password" placeholder={ this.translation('passwordConfirm') } className={`text-input half-input ${this.state.fields.passwordConfirm.highlight ? 'highlight' : ''}`} onChange={e => this.handleChange(e, 'passwordConfirm')}></input>
                            </div>
                            <button className="btn btn-complete" onClick={e => this.nextStep(e, 3)}>{ this.translation('create') }</button>
                        </div>
                        <div className={this.state.step !== 3 ? 'hide' : ''}>
                            <div className="row">
                                <img src="/assets/success.gif" className="message success" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

export default Form;

